import replayutils.gokzreplay as gokz
import replayutils.kztreplay as kzt

def main():
	replayData = kzt.import_replay_data_as_gokz("E:/coding/python/democheck/demos/wilito/kz_slidemap_ez_tp.rec")
	gokz.export_replay(replayData, "E:/coding/python/democheck/export_test.replay")

if __name__ == "__main__":
	main()