import struct

# reads 32 bit ints from file (as little endian)
def read_int32(File):
	return struct.unpack("<i", File.read(4))[0]

def read_uint8(File):
	#return int.from_bytes(File.read(1), byteorder="little")
	return struct.unpack("<B", File.read(1))[0]

def read_float(File):
	return struct.unpack("f", File.read(4))[0]

def read_vector(File):
	return read_float(File), read_float(File), read_float(File)


def write_int32(File, number: int):
	File.write(struct.pack("<i", number))

def write_uint8(File, number: int):
	File.write(struct.pack("<B", number))

def write_float(File, number: float):
	File.write(struct.pack("f", number))

def write_vector(File, vector: list):
	for i in range(vector):
		write_float(File, vector[i])