import replayutils.kzt_constants as kzt
import replayutils.filestuff as filestuff
import replayutils.gokzreplay as gokz

import ntpath
import os

from replayutils.csgo_constants import *

class ReplayData:
	magicNumber = -1
	formatVersion = -1
	IP = ""
	steamID2 = ""
	timeString = ""
	playerName = ""
	checkpoints = -1
	initialPosition = []
	initialAngles = []
	replayLength = -1
	tickData = []
	additionalTeleport = []

def import_replay_data(path: str) -> ReplayData:
	replayData = ReplayData
	
	fp = open(path, 'rb')
	
	replayData.magicNumber = filestuff.read_int32(fp)
	if replayData.magicNumber != kzt.REPLAY_MAGIC_NUMBER:
		print("[KZTimer] Tried to load invalid replay file: \"{0}\"".format(path))
		return False
	
	replayData.formatVersion = filestuff.read_uint8(fp)
	if replayData.formatVersion > kzt.REPLAY_FORMAT_VERSION:
		print("[KZTimer] Tried to load replay file with unsupported format version \"{0}\":  \"{1}\"".format(replayData.formatVersion, path))
		return False
	
	if replayData.formatVersion == 0x02:
		# IP
		strlen = filestuff.read_uint8(fp)
		replayData.IP = fp.read(strlen).decode("utf-8")
		
		# SteamID2
		strlen = filestuff.read_uint8(fp)
		replayData.steamID2 = fp.read(strlen).decode("utf-8")
	
	# Time string
	strlen = filestuff.read_uint8(fp)
	replayData.timeString = fp.read(strlen).decode("utf-8")
	
	# Player name
	strlen = filestuff.read_uint8(fp)
	replayData.playerName = fp.read(strlen).decode("utf-8")
	
	replayData.checkpoints = filestuff.read_int32(fp)
	replayData.initialPosition = filestuff.read_vector(fp)
	replayData.initialAngles = [filestuff.read_float(fp), filestuff.read_float(fp)]
	
	replayData.replayLength = filestuff.read_int32(fp)
	replayData.tickData = []
	replayData.additionalTeleport = []
	
	# tickdata
	for i in range(replayData.replayLength):
		
		replayData.tickData += [[0] * kzt.TICKDATA_SIZE]
		
		# read tickdata
		for j in range(kzt.TICKDATA_SIZE):
			if j >= 2 and j <= 9:
				replayData.tickData[i][j] = filestuff.read_float(fp)
			else:
				replayData.tickData[i][j] = filestuff.read_int32(fp)
		
		# check if we need any additional teleport data
		addTpFlags = replayData.tickData[i][kzt.TICKDATA_ADDTP]
		if addTpFlags & kzt.ADDTP_FULLBITMASK:
			addTp = [0] * kzt.ADDTP_SIZE
		
			if addTpFlags & kzt.ADDTP_TELEPORTED_ORIGIN:
				addTp[kzt.ADDTP_ORIGIN_X:kzt.ADDTP_ORIGIN_Z+1] = filestuff.read_vector(fp)
			
			if addTpFlags & kzt.ADDTP_TELEPORTED_ANGLES:
				addTp[kzt.ADDTP_ANGLES_PITCH:kzt.ADDTP_ANGLES_ROLL+1] = filestuff.read_vector(fp)
			
			if addTpFlags & kzt.ADDTP_TELEPORTED_VELOCITY:
				addTp[kzt.ADDTP_VELOCITY_X:kzt.ADDTP_VELOCITY_Z+1] = filestuff.read_vector(fp)
			
			addTp[kzt.ADDTP_FLAGS] = addTpFlags & kzt.ADDTP_FULLBITMASK
			replayData.additionalTeleport.append(addTp)
	
	fp.close()
	
	print("[KZTimer] Replay successfully imported!")
	
	return replayData

def import_replay_data_as_gokz(path: str) -> gokz.ReplayData:
	# unused variables
	"""
	gokzVersion
	mapName
	steamAccountID
	"""
	
	# unused tickdata
	"""
	origin only gets updated every 150 ticks
	TICKDATA_ORIGIN_X
	TICKDATA_ORIGIN_Y
	TICKDATA_ORIGIN_Z
	TICKDATA_FLAGS
	"""
	replayData = gokz.ReplayData
	
	replayData.course = 0 # kztimer doesn't support bonuses
	replayData.mode = 2 # 2 is kzt
	replayData.style = 0 # idk what style is used for https://bitbucket.org/kztimerglobalteam/gokz/src/d16fdcb3eda567e58505597394f64954811fc173/addons/sourcemod/scripting/include/gokz/core.inc?at=master#lines-85
	
	fp = open(path, 'rb')
	
	replayData.magicNumber = filestuff.read_int32(fp)
	if replayData.magicNumber != kzt.REPLAY_MAGIC_NUMBER:
		print("[KZTimer] Tried to load invalid replay file: \"{0}\"".format(path))
		return False
	
	replayData.formatVersion = filestuff.read_uint8(fp)
	if replayData.formatVersion > kzt.REPLAY_FORMAT_VERSION:
		print("[KZTimer] Tried to load replay file with unsupported format version \"{0}\":  \"{1}\"".format(replayData.formatVersion, path))
		return False
	
	if replayData.formatVersion == 0x02:
		# IP
		strlen = filestuff.read_uint8(fp)
		replayData.IP = fp.read(strlen).decode("utf-8")
		
		# SteamID2
		strlen = filestuff.read_uint8(fp)
		replayData.steamID2 = fp.read(strlen).decode("utf-8")
		# can't be arsed to parse this into gokz.ReplayData.steamAccountID
	
	# Time string
	strlen = filestuff.read_uint8(fp)
	fp.read(strlen).decode("utf-8")
	
	# Player name
	strlen = filestuff.read_uint8(fp)
	replayData.playerName = fp.read(strlen).decode("utf-8")
	
	replayData.teleportsUsed = filestuff.read_int32(fp) # checkpoints
	
	# initial position, used for keeping track of addtp TODO: fix/remove this
	initialPosition = filestuff.read_vector(fp)
	
	# can't use this data for gokz
	filestuff.read_float(fp)  # initial yaw
	filestuff.read_float(fp)  # initial pitch
	
	replayData.replayLength = filestuff.read_int32(fp)
	replayData.tickData = []
	
	# kztimer replays contain no information about tickrate. assume 128 tick.
	replayData.time = float(replayData.replayLength) / 128
	
	# helper variables
	kztTickData = []
	addTpOrigins = [[0, initialPosition]] # save initial position here for the reconstruction
	
	# tickdata
	for i in range(replayData.replayLength):
		replayData.tickData += [[0] * gokz.TICKDATA_SIZE]
		kztTickData += [[0] * kzt.TICKDATA_SIZE]
		
		# read  kzt tickdata
		for j in range(kzt.TICKDATA_SIZE):
			if j >= 2 and j <= 9:
				kztTickData[i][j] = filestuff.read_float(fp)
			else:
				kztTickData[i][j] = filestuff.read_int32(fp)
		
		# read kzt additional tp
		addTpFlags = kztTickData[i][kzt.TICKDATA_ADDTP]
		if addTpFlags & kzt.ADDTP_FULLBITMASK:
			# first reconstruction pass. record ticks and origins where ADDTP was used
			if addTpFlags & kzt.ADDTP_TELEPORTED_ORIGIN:
				origin = filestuff.read_vector(fp)
				addTpOrigins += [[i, origin]]
				# save recorded origin, so we don't have to bother with this later
				replayData.tickData[i][gokz.TICKDATA_ORIGIN_X:gokz.TICKDATA_ORIGIN_Z+1] = origin
			
			# unused
			if addTpFlags & kzt.ADDTP_TELEPORTED_ANGLES:
				filestuff.read_vector(fp)
			
			# unused
			if addTpFlags & kzt.ADDTP_TELEPORTED_VELOCITY:
				filestuff.read_vector(fp)
		
		scuffedFlags = 0
		
		if kztTickData[i][kzt.TICKDATA_VELOCITY_Z] == 0.0:
			scuffedFlags |= FL_ONGROUND
		
		# copy kzt data to gokz
		# don't copy origin just yet, we need to interpolate it first
		#replayData.tickData[i][gokz.TICKDATA_ORIGIN_X:gokz.TICKDATA_ORIGIN_Z+1] = currentOrigin[:] # scuffed origin
		replayData.tickData[i][gokz.TICKDATA_ANGLES_YAW] = kztTickData[i][kzt.TICKDATA_ANGLES_YAW]
		replayData.tickData[i][gokz.TICKDATA_ANGLES_PITCH] = kztTickData[i][kzt.TICKDATA_ANGLES_PITCH]
		replayData.tickData[i][gokz.TICKDATA_BUTTONS] = kztTickData[i][kzt.TICKDATA_BUTTONS]
		replayData.tickData[i][gokz.TICKDATA_FLAGS] = scuffedFlags
	
	fp.close()
	
	replayData.tickData[0][gokz.TICKDATA_ORIGIN_X:gokz.TICKDATA_ORIGIN_Z+1] = initialPosition
	
	# origin reconstruction! yay!
	for i in range(len(addTpOrigins)):
		diff = addTpOrigins[i][0] - addTpOrigins[i - 1][0]
		
		if i > 0 and diff > 1:
			origin = list(addTpOrigins[i - 1][1])
			for j in range(addTpOrigins[i - 1][0] + 1, addTpOrigins[i][0]):
				origin[0] += kztTickData[j][kzt.TICKDATA_VELOCITY_X] / 128
				origin[1] += kztTickData[j][kzt.TICKDATA_VELOCITY_Y] / 128
				origin[2] += kztTickData[j][kzt.TICKDATA_VELOCITY_Z] / 128
				
				replayData.tickData[j][gokz.TICKDATA_ORIGIN_X:gokz.TICKDATA_ORIGIN_Z+1] = origin
			
	
	# final stuff
	replayData.magicNumber = gokz.REPLAY_MAGIC_NUMBER
	replayData.formatVersion = gokz.REPLAY_FORMAT_VERSION
	replayData.gokzVersion = "0.05"
	replayData.mapName = os.path.splitext(ntpath.basename(fp.name))[0]
	
	print("[KZTimer] Replay successfully imported!")
	
	return replayData