# replay constants
REPLAY_MAGIC_NUMBER = 0x676F6B7A
REPLAY_FORMAT_VERSION = 0x01

TICKDATA_ORIGIN_X, \
TICKDATA_ORIGIN_Y, \
TICKDATA_ORIGIN_Z, \
TICKDATA_ANGLES_PITCH, \
TICKDATA_ANGLES_YAW, \
TICKDATA_BUTTONS, \
TICKDATA_FLAGS, \
TICKDATA_SIZE = range(8)