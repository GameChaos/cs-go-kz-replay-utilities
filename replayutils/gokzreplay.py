from replayutils.gokz_constants import *
import replayutils.filestuff as filestuff

class ReplayData:
	magicNumber = -1
	formatVersion = -1
	gokzVersion = ""
	mapName = ""
	course = -1
	mode = -1
	style = -1
	time = -1
	teleportsUsed = -1
	steamAccountID = -1
	steamID2 = ""
	IP = ""
	playerName = ""
	replayLength = -1
	tickData = [[0] * 7]

def import_replay_data(path: str, only_header = False):
	replayData = ReplayData
	
	fp = open(path, 'rb')
	
	replayData.magicNumber = filestuff.read_int32(fp)
	if replayData.magicNumber != REPLAY_MAGIC_NUMBER:
		print("[GOKZ] Tried to load invalid replay file: \"{0}\"".format(path))
		return False
	
	replayData.formatVersion = filestuff.read_uint8(fp)
	if replayData.formatVersion != REPLAY_FORMAT_VERSION:
		print("[GOKZ] Tried to load replay file with unsupported format version \"{0}\":  \"{1}\"".format(replayData.formatVersion, path))
		return False
	
	# GOKZ version
	strlen = filestuff.read_uint8(fp)
	replayData.gokzVersion = fp.read(strlen).decode("utf-8")
	
	# Map name
	strlen = filestuff.read_uint8(fp)
	replayData.mapName = fp.read(strlen).decode("utf-8")
	
	# Some integers...
	replayData.course = filestuff.read_int32(fp)
	replayData.mode = filestuff.read_int32(fp)
	replayData.style = filestuff.read_int32(fp)
	
	# Time
	replayData.time = filestuff.read_float(fp)
	
	# Some integers...
	replayData.teleportsUsed = filestuff.read_int32(fp)
	replayData.steamAccountID = filestuff.read_int32(fp)
	
	# SteamID2
	strlen = filestuff.read_uint8(fp)
	replayData.steamID2 = fp.read(strlen).decode("utf-8")
	
	# IP
	strlen = filestuff.read_uint8(fp)
	replayData.IP = fp.read(strlen).decode("utf-8")
	
	# Player name
	strlen = filestuff.read_uint8(fp)
	replayData.playerName = fp.read(strlen).decode("utf-8")
	
	# Read tick data
	replayData.replayLength = filestuff.read_int32(fp)
	
	replayData.tickData = []
	if not only_header:
		for i in range(replayData.replayLength):
			replayData.tickData += [[0] * TICKDATA_SIZE]
			replayData.tickData[i][0] = filestuff.read_float(fp) # origin[0]
			replayData.tickData[i][1] = filestuff.read_float(fp) # origin[1]
			replayData.tickData[i][2] = filestuff.read_float(fp) # origin[2]
			replayData.tickData[i][3] = filestuff.read_float(fp) # angles[0]
			replayData.tickData[i][4] = filestuff.read_float(fp) # angles[1]
			replayData.tickData[i][5] = filestuff.read_int32(fp) # buttons
			replayData.tickData[i][6] = filestuff.read_int32(fp) # flags
	
	fp.close()
	
	print("[GOKZ] Replay successfully imported!")
	
	return replayData

def export_replay(replayData: ReplayData, path: str):
	fp = open(path, "wb")
	
	filestuff.write_int32(fp, REPLAY_MAGIC_NUMBER)
	filestuff.write_uint8(fp, REPLAY_FORMAT_VERSION)
	
	# GOKZ version
	filestuff.write_uint8(fp, len(replayData.gokzVersion))
	fp.write(replayData.gokzVersion.encode("utf-8"))
	
	# Map name
	filestuff.write_uint8(fp, len(replayData.mapName))
	fp.write(replayData.mapName.encode("utf-8"))
	
	# Some integers...
	filestuff.write_int32(fp, replayData.course)
	filestuff.write_int32(fp, replayData.mode)
	filestuff.write_int32(fp, replayData.style)
	
	# Time
	filestuff.write_float(fp, replayData.time)
	
	# Some integers...
	filestuff.write_int32(fp, replayData.teleportsUsed)
	filestuff.write_int32(fp, replayData.steamAccountID)
	
	# SteamID2
	filestuff.write_uint8(fp, len(replayData.steamID2))
	fp.write(replayData.steamID2.encode("utf-8"))
	
	# IP
	filestuff.write_uint8(fp, len(replayData.IP))
	fp.write(replayData.IP.encode("utf-8"))
	
	# Player name
	filestuff.write_uint8(fp, len(replayData.playerName))
	fp.write(replayData.playerName.encode("utf-8"))
	
	# Tick data
	filestuff.write_int32(fp, replayData.replayLength)
	
	for i in range(replayData.replayLength):
		filestuff.write_float(fp, replayData.tickData[i][0])
		filestuff.write_float(fp, replayData.tickData[i][1])
		filestuff.write_float(fp, replayData.tickData[i][2])
		filestuff.write_float(fp, replayData.tickData[i][3])
		filestuff.write_float(fp, replayData.tickData[i][4])
		filestuff.write_int32(fp, replayData.tickData[i][5])
		filestuff.write_int32(fp, replayData.tickData[i][6])
	fp.close()
	
	print("[GOKZ] Replay successfully exported!")

